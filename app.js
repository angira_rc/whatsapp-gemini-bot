require('dotenv').config()

const mongoose = require('mongoose')

const qrcode = require('qrcode-terminal');
const { GoogleGenerativeAI } = require("@google/generative-ai")

const { Client } = require('whatsapp-web.js');

const { Chat } = require('./models/chat');

const { GEMINI_API_KEY, MONGODB_URI, MY_ID } = process.env

const app = new GoogleGenerativeAI(GEMINI_API_KEY)
const model = app.getGenerativeModel({ model: "gemini-pro" })

mongoose.connect(MONGODB_URI)
    .then(async () => {
        console.log('DB Connected!')
        
        const client = new Client()

        client.on('qr', (qr) => {
            qrcode.generate(qr, { small: true });
        });

        client.on('')
        
        client.on('ready', () => {
            console.log('Client is ready!');
        });
        
        client.on('message', async (message) => {
            console.log({ author: message.author, type: message.type })
            if (message.author) {
                if (message.mentionedIds.includes(MY_ID)) {                
                    const prompt = message.body
                
                    const result = await model.generateContent(prompt)
                
                    const response = result.response
                    const text = response.text()

                    await client.sendMessage(message.from, text)
                }
            } else if (message.type === 'chat') {
                let chat = await Chat.findOne({ phone: message.from })

                let history = chat?.history?.map(JSON.parse) ?? []

                const newChat = model.startChat({ history })
            
                const msg = message.body
            
                const result = await newChat.sendMessage(msg)
            
                const response = result.response
            
                const text = response.text()
            
                await message.reply(text)
            }
        });
        
        client.initialize();
    })
    .catch(err => console.log(err))