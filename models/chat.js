const mongoose = require('mongoose')
const Schema = mongoose.Schema

const chat = new Schema({
    phone: {
        type: String,
        required: true
    },
    groupId: {
        type: String
    },
    history: [String],
})

module.exports = {
    'Chat': mongoose.model('Chat', chat)
}